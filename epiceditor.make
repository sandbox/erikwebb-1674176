core = 7.x
api = 2

libraries[epiceditor][download][type] = "get"
libraries[epiceditor][download][url] = "https://github.com/OscarGodson/EpicEditor/archive/0.2.1.1.tar.gz"
libraries[epiceditor][download][md5] = "83b2880d4ec7a9f7d089c7143ca74480"
libraries[epiceditor][directory_name] = "epiceditor"