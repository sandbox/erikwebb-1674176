<?php

/**
 * @file
 * Administrative forms for EpicEditor module.
 */

/**
 * 
 */
function epiceditor_admin_form($form, &$form_state) {
  $form = array();

  $form['epiceditor_themes'] = array(
		'#type' => 'fieldset',
	  '#title' => t('Editor Themes'),
		'#weight' => 5,
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	);


  // @todo: theme options are currently hardcoded. These should be loaded from the file system
	$form['epiceditor_themes']['epiceditor_editor_theme'] = array(
       '#type' => 'select',
       '#title' => t('Editor'),
       '#options' => array(
          'epic-dark' => t('Epic Dark'),
					'epic-light' => t('Epic Light'),
       ),
       '#default_value' => variable_get('epiceditor_editor_theme', 'epic-dark'),
       '#description' => t('Theme for editor'),
   );

	$form['epiceditor_themes']['epiceditor_preview_theme'] = array(
       '#type' => 'select',
       '#title' => t('Preview'),
       '#options' => array(
          'github' => t('github'),
					'preview-dark' => t('Epic Dark'),
       ),
       '#default_value' => variable_get('epiceditor_preview_theme', 'github'),
       '#description' => t('Theme for preview'),
   );

  return system_settings_form($form);
}
