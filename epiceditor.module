<?php

/**
 * Implements hook_menu().
 */
function epiceditor_menu() {
  $items = array();

  $items['admin/config/content/epiceditor'] = array(
    'title' => 'EpicEditor configuration',
    'description' => 'Configure integration with EpicEditor.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('epiceditor_admin_form'),
    'access arguments' => array('administer epiceditor'),
    'file' => 'epiceditor.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function epiceditor_permission() {
  return array(
    'administer epiceditor' => array(
      'title' => t('Administer EpicEditor'),
      'description' => t('Configure options for EpicEditor.'),
    ),
  );
}

/**
 * Implements hook_filter_info_alter().
 */
function epiceditor_filter_info_alter(&$info) {
  $info['filter_markdown']['settings callback'] = '_epiceditor_filter_settings';
}

/**
 * Implements hook_element_info_alter().
 */
function epiceditor_element_info_alter(&$types) {
  $types['text_format']['#pre_render'][] = 'epiceditor_pre_render_text_format';
}

/**
 * Process a text format widget to load and attach EpicEditor, if configured.
 *
 * Borrowed heavily from WYSIWYG API.
 */
function epiceditor_pre_render_text_format($element) {
  global $base_url;

  // filter_process_format() copies properties to the expanded 'value' child
  // element. Skip this text format widget, if it contains no 'format' or when
  // the current user does not have access to edit the value.
  if (!isset($element['format']) || !empty($element['value']['#disabled'])) {
    return $element;
  }

  // Determine formats where EpicEditor is available.
  $epiceditor_formats = array();

  $format_field = &$element['format'];

  // Determine the available text formats.
  foreach ($format_field['format']['#options'] as $format_id => $format_name) {
    $filters = filter_list_format($format_id);
    if (isset($filters['filter_markdown'])) {
      if (!empty($filters['filter_markdown']->settings['enable_epiceditor'])) {
        $epiceditor_formats[] = $format_id;

        // Load Drupal-specific JavaScript and CSS
        drupal_add_js(drupal_get_path('module', 'epiceditor') . '/epiceditor.js');
        drupal_add_css(drupal_get_path('module', 'epiceditor') . '/epiceditor.css');
        // Load main EpicEditor library
        if ($path = libraries_get_path('epiceditor')) {
          drupal_add_js($path . '/epiceditor/js/epiceditor.min.js');
          // @todo: this basePath probably won't work with subfolder docroots
          drupal_add_js(array(
            'epiceditor' => array(
              'settings' => array(
                'basePath' => '/' . $path . '/epiceditor',
                'previewTheme' => variable_get('epiceditor_preview_theme', 'github'),
                'editorTheme' => variable_get('epiceditor_editor_theme', 'epic-dark'),
              )
            )
          ), 'setting');
        }
      }
    }
  }

  if (count($epiceditor_formats)) {
    $settings = array(
      'formats' => $epiceditor_formats,
    );
    drupal_add_js(array(
      'epiceditor' => array(
        'fields' => array(
          $element['value']['#id'] => $settings
        )
      )
    ) ,'setting');
  }

  return $element;
}

/**
 * Override filter settings for Markdown to add additional setting for
 * enabling Epic Editor.
 */
function _epiceditor_filter_settings($form, &$form_state, $filter, $format, $defaults) {
  // Ensure Markdown module is fully loaded (probably not necessary)
  module_load_include('module', 'markdown');

  $settings = _filter_markdown_settings($form, $form_state, $filter, $format, $defaults);
  $settings['enable_epiceditor'] = array(
    '#title' => t('Use EpicEditor'),
    '#type' => 'checkbox',
    '#description' => t('<a href="@epiceditor_url">EpicEditor</a> is an
        embeddable JavaScript Markdown editor with split fullscreen editing,
        live previewing, automatic draft saving, offline support, and more.',
      array('@epiceditor_url' => 'http://epiceditor.com/')),
    '#default_value' => isset($filter->settings['enable_epiceditor']) ? $filter->settings['enable_epiceditor'] : false,
  );

  return $settings;
}
