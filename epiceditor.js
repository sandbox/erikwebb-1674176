(function($) {
  Drupal.behaviors.epicEditor = {
    attach: function (context, settings) {

      // generate a random/unquie ID for localStorage
      var localFilePrefix = 'epiceditor-' + (new Date()).getTime();

      jQuery.each(settings.epiceditor.fields, function(fieldId, settings) {
        // Insert div for EpicEditor to take over while leaving Drupal's textarea alone
        $('#'+fieldId).parent().before('<div id="epiceditor-'+fieldId+'" class="epiceditor-wrapper"></div>');
        // Hide Drupal's textarea wrapper
        $('#'+fieldId).parent().hide();

        var opts = {
          container: 'epiceditor-'+fieldId,
          basePath: Drupal.settings.epiceditor.settings.basePath,
          file: {
            name: localFilePrefix+'-'+fieldId,
            defaultContent: $('#'+fieldId).val(),
            autoSave: 100
          },
          theme: {
            base:'/themes/base/epiceditor.css',
            preview:'/themes/preview/'+Drupal.settings.epiceditor.settings.previewTheme+'.css',
            editor:'/themes/editor/'+Drupal.settings.epiceditor.settings.editorTheme+'.css'
          },
          focusOnLoad: false
        }
        var editor = new EpicEditor(opts);
        editor.load();
        editor.on('save', function () {
          $('#'+fieldId).val(editor.exportFile());
        });
        $('#'+fieldId).closest('form')
        .bind('submit', function() {
          editor.save().remove(localFilePrefix+'-'+fieldId);
        });

        // Remove local file if page is left without saving
        $(window).bind('beforeunload', function(){
          editor.remove(localFilePrefix+'-'+fieldId);
        });
      });
    }
  };
})(jQuery);