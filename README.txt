INTRODUCTION
------------

EpicEditor module allows use of the EpicEditor (http://epiceditor.com/) for
Drupal filtered text fields.

This module requires:
  - Markdown Filter (http://drupal.org/project/markdown/)
  - Libraries API (http://drupal.org/project/libraries/)

INSTALLATION
------------

1. Copy the epiceditor directory to your sites/SITENAME/modules or sites/all/modules directory.

2. Download the EpicEditor from http://epiceditor.com/#download (git clone git@github.com:OscarGodson/EpicEditor) and place in sites/SITENAME/libraries or sites/all/libraries. 
    The directory should contain another epiceditor directory, such as this:
    libraries/
      epiceditor/
        epiceditor/
          js/
          images/
          themes/
        README.md
        [...]

3. Enable the module at /admin/modules.

4. Enable the EpicEditor by selecting the "Use EpicEditor" checkbox on the Markdown tab in Filter settings for each text format needed. Example: /admin/config/content/formats/markdown

5. Additional settings can be configured at /admin/config/content/epiceditor